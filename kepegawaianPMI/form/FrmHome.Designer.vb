﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmHome
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(FrmHome))
        Me.TabControl1 = New System.Windows.Forms.TabControl()
        Me.tabHome = New System.Windows.Forms.TabPage()
        Me.TabPegawai = New System.Windows.Forms.TabPage()
        Me.TabLaporan = New System.Windows.Forms.TabPage()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.GroupBox3 = New System.Windows.Forms.GroupBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.GroupBoxData = New System.Windows.Forms.GroupBox()
        Me.btnBiodataPegawai = New System.Windows.Forms.Button()
        Me.btnIzin = New System.Windows.Forms.Button()
        Me.btnCuti = New System.Windows.Forms.Button()
        Me.DataGridView1 = New System.Windows.Forms.DataGridView()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.Button2 = New System.Windows.Forms.Button()
        Me.Button3 = New System.Windows.Forms.Button()
        Me.TabControl1.SuspendLayout()
        Me.tabHome.SuspendLayout()
        Me.TabPegawai.SuspendLayout()
        Me.GroupBox1.SuspendLayout()
        Me.GroupBox3.SuspendLayout()
        Me.GroupBoxData.SuspendLayout()
        CType(Me.DataGridView1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'TabControl1
        '
        Me.TabControl1.Controls.Add(Me.tabHome)
        Me.TabControl1.Controls.Add(Me.TabPegawai)
        Me.TabControl1.Controls.Add(Me.TabLaporan)
        Me.TabControl1.Location = New System.Drawing.Point(12, 12)
        Me.TabControl1.Name = "TabControl1"
        Me.TabControl1.SelectedIndex = 0
        Me.TabControl1.Size = New System.Drawing.Size(811, 511)
        Me.TabControl1.TabIndex = 0
        '
        'tabHome
        '
        Me.tabHome.Controls.Add(Me.Label8)
        Me.tabHome.Controls.Add(Me.GroupBox3)
        Me.tabHome.Controls.Add(Me.GroupBox2)
        Me.tabHome.Controls.Add(Me.GroupBox1)
        Me.tabHome.Location = New System.Drawing.Point(4, 22)
        Me.tabHome.Name = "tabHome"
        Me.tabHome.Padding = New System.Windows.Forms.Padding(3)
        Me.tabHome.Size = New System.Drawing.Size(803, 485)
        Me.tabHome.TabIndex = 0
        Me.tabHome.Text = "Home"
        Me.tabHome.UseVisualStyleBackColor = True
        '
        'TabPegawai
        '
        Me.TabPegawai.Controls.Add(Me.btnCuti)
        Me.TabPegawai.Controls.Add(Me.btnIzin)
        Me.TabPegawai.Controls.Add(Me.btnBiodataPegawai)
        Me.TabPegawai.Controls.Add(Me.GroupBoxData)
        Me.TabPegawai.Location = New System.Drawing.Point(4, 22)
        Me.TabPegawai.Name = "TabPegawai"
        Me.TabPegawai.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPegawai.Size = New System.Drawing.Size(803, 485)
        Me.TabPegawai.TabIndex = 1
        Me.TabPegawai.Text = "Data Pegawai"
        Me.TabPegawai.UseVisualStyleBackColor = True
        '
        'TabLaporan
        '
        Me.TabLaporan.Location = New System.Drawing.Point(4, 22)
        Me.TabLaporan.Name = "TabLaporan"
        Me.TabLaporan.Padding = New System.Windows.Forms.Padding(3)
        Me.TabLaporan.Size = New System.Drawing.Size(803, 485)
        Me.TabLaporan.TabIndex = 2
        Me.TabLaporan.Text = "Laporan"
        Me.TabLaporan.UseVisualStyleBackColor = True
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.Label1)
        Me.GroupBox1.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox1.Location = New System.Drawing.Point(13, 13)
        Me.GroupBox1.Margin = New System.Windows.Forms.Padding(10)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Padding = New System.Windows.Forms.Padding(10)
        Me.GroupBox1.Size = New System.Drawing.Size(777, 100)
        Me.GroupBox1.TabIndex = 1
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Visi"
        '
        'GroupBox2
        '
        Me.GroupBox2.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox2.Location = New System.Drawing.Point(13, 175)
        Me.GroupBox2.Margin = New System.Windows.Forms.Padding(10)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Padding = New System.Windows.Forms.Padding(10)
        Me.GroupBox2.Size = New System.Drawing.Size(472, 80)
        Me.GroupBox2.TabIndex = 2
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "Misi"
        '
        'Label1
        '
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(13, 32)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(446, 56)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "PMI yang berkarakter, profesional, dan dicintai masyarakat"
        '
        'GroupBox3
        '
        Me.GroupBox3.Controls.Add(Me.Label7)
        Me.GroupBox3.Controls.Add(Me.Label6)
        Me.GroupBox3.Controls.Add(Me.Label5)
        Me.GroupBox3.Controls.Add(Me.Label4)
        Me.GroupBox3.Controls.Add(Me.Label2)
        Me.GroupBox3.Controls.Add(Me.Label3)
        Me.GroupBox3.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox3.Location = New System.Drawing.Point(13, 133)
        Me.GroupBox3.Margin = New System.Windows.Forms.Padding(10)
        Me.GroupBox3.Name = "GroupBox3"
        Me.GroupBox3.Padding = New System.Windows.Forms.Padding(10)
        Me.GroupBox3.Size = New System.Drawing.Size(777, 281)
        Me.GroupBox3.TabIndex = 2
        Me.GroupBox3.TabStop = False
        Me.GroupBox3.Text = "Misi"
        '
        'Label3
        '
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(60, 33)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(704, 89)
        Me.Label3.TabIndex = 1
        Me.Label3.Text = resources.GetString("Label3.Text")
        '
        'Label2
        '
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(60, 122)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(704, 89)
        Me.Label2.TabIndex = 2
        Me.Label2.Text = "Meningkatkan kemandirian organisasi PMI melalui kemitraan strategis yang berksesi" & _
            "nambungan dengan pemerintah, swasta, mitra gerakan, dan pemangku kepentingan lai" & _
            "nnya di semua tingkatan."
        '
        'Label4
        '
        Me.Label4.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.Location = New System.Drawing.Point(61, 221)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(704, 54)
        Me.Label4.TabIndex = 3
        Me.Label4.Text = "Meningkatkan reputasi organisasi PMI di tingkat Nasional dan Internasional."
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.Location = New System.Drawing.Point(14, 33)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(20, 18)
        Me.Label5.TabIndex = 4
        Me.Label5.Text = "a."
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.Location = New System.Drawing.Point(13, 122)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(20, 18)
        Me.Label6.TabIndex = 5
        Me.Label6.Text = "b."
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label7.Location = New System.Drawing.Point(14, 221)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(20, 18)
        Me.Label7.TabIndex = 6
        Me.Label7.Text = "c."
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label8.Location = New System.Drawing.Point(27, 424)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(342, 18)
        Me.Label8.TabIndex = 4
        Me.Label8.Text = "Jl. Imam Bonjol Km. 3 No. 182 Denpasar-Bali 8011"
        '
        'GroupBoxData
        '
        Me.GroupBoxData.Controls.Add(Me.Button1)
        Me.GroupBoxData.Controls.Add(Me.Button2)
        Me.GroupBoxData.Controls.Add(Me.Button3)
        Me.GroupBoxData.Controls.Add(Me.DataGridView1)
        Me.GroupBoxData.Location = New System.Drawing.Point(127, 6)
        Me.GroupBoxData.Name = "GroupBoxData"
        Me.GroupBoxData.Size = New System.Drawing.Size(670, 473)
        Me.GroupBoxData.TabIndex = 0
        Me.GroupBoxData.TabStop = False
        Me.GroupBoxData.Text = "Biodata Pegawai"
        '
        'btnBiodataPegawai
        '
        Me.btnBiodataPegawai.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnBiodataPegawai.Location = New System.Drawing.Point(6, 6)
        Me.btnBiodataPegawai.Name = "btnBiodataPegawai"
        Me.btnBiodataPegawai.Size = New System.Drawing.Size(115, 29)
        Me.btnBiodataPegawai.TabIndex = 1
        Me.btnBiodataPegawai.Text = "Biodata Pegawai"
        Me.btnBiodataPegawai.UseVisualStyleBackColor = True
        '
        'btnIzin
        '
        Me.btnIzin.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnIzin.Location = New System.Drawing.Point(6, 41)
        Me.btnIzin.Name = "btnIzin"
        Me.btnIzin.Size = New System.Drawing.Size(115, 29)
        Me.btnIzin.TabIndex = 2
        Me.btnIzin.Text = "Permohonan Izin"
        Me.btnIzin.UseVisualStyleBackColor = True
        '
        'btnCuti
        '
        Me.btnCuti.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnCuti.Location = New System.Drawing.Point(6, 76)
        Me.btnCuti.Name = "btnCuti"
        Me.btnCuti.Size = New System.Drawing.Size(115, 29)
        Me.btnCuti.TabIndex = 3
        Me.btnCuti.Text = "Pengajuan Cuti"
        Me.btnCuti.UseVisualStyleBackColor = True
        '
        'DataGridView1
        '
        Me.DataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DataGridView1.Location = New System.Drawing.Point(6, 70)
        Me.DataGridView1.Name = "DataGridView1"
        Me.DataGridView1.Size = New System.Drawing.Size(658, 397)
        Me.DataGridView1.TabIndex = 0
        '
        'Button1
        '
        Me.Button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button1.Location = New System.Drawing.Point(307, 35)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(115, 29)
        Me.Button1.TabIndex = 6
        Me.Button1.Text = "Add"
        Me.Button1.UseVisualStyleBackColor = True
        '
        'Button2
        '
        Me.Button2.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button2.Location = New System.Drawing.Point(428, 35)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(115, 29)
        Me.Button2.TabIndex = 5
        Me.Button2.Text = "Edit"
        Me.Button2.UseVisualStyleBackColor = True
        '
        'Button3
        '
        Me.Button3.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button3.Location = New System.Drawing.Point(549, 35)
        Me.Button3.Name = "Button3"
        Me.Button3.Size = New System.Drawing.Size(115, 29)
        Me.Button3.TabIndex = 4
        Me.Button3.Text = "Delete"
        Me.Button3.UseVisualStyleBackColor = True
        '
        'FrmHome
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(835, 535)
        Me.Controls.Add(Me.TabControl1)
        Me.Name = "FrmHome"
        Me.Text = "Home"
        Me.TabControl1.ResumeLayout(False)
        Me.tabHome.ResumeLayout(False)
        Me.tabHome.PerformLayout()
        Me.TabPegawai.ResumeLayout(False)
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox3.ResumeLayout(False)
        Me.GroupBox3.PerformLayout()
        Me.GroupBoxData.ResumeLayout(False)
        CType(Me.DataGridView1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents TabControl1 As System.Windows.Forms.TabControl
    Friend WithEvents tabHome As System.Windows.Forms.TabPage
    Friend WithEvents TabPegawai As System.Windows.Forms.TabPage
    Friend WithEvents TabLaporan As System.Windows.Forms.TabPage
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents GroupBox3 As System.Windows.Forms.GroupBox
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents btnCuti As System.Windows.Forms.Button
    Friend WithEvents btnIzin As System.Windows.Forms.Button
    Friend WithEvents btnBiodataPegawai As System.Windows.Forms.Button
    Friend WithEvents GroupBoxData As System.Windows.Forms.GroupBox
    Friend WithEvents DataGridView1 As System.Windows.Forms.DataGridView
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents Button2 As System.Windows.Forms.Button
    Friend WithEvents Button3 As System.Windows.Forms.Button

End Class
