﻿Public Class FrmHome

    Private Sub FrmHome_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

    End Sub


    Private Sub btnBiodataPegawai_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBiodataPegawai.Click
        loadData(DATA_TYPE_LOADER_BIODATA)
    End Sub

    Private Sub loadData(ByVal dataLoader As String)
        Dim title As String = vbNullString
        Select Case dataLoader
            Case DATA_TYPE_LOADER_BIODATA
                title = "Biodata Pegawai"
            Case DATA_TYPE_LOADER_CUTI
                title = "Permohonan Izin"
            Case DATA_TYPE_LOADER_IZIN
                title = "Pengajuan Cuti"
        End Select
        GroupBoxData.Text = title
    End Sub

    Private Sub btnIzin_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnIzin.Click
        loadData(DATA_TYPE_LOADER_IZIN)
    End Sub

    Private Sub btnCuti_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCuti.Click
        loadData(DATA_TYPE_LOADER_CUTI)
    End Sub
End Class
